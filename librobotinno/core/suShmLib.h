/*
 ============================================================================
 Name        : suShmLib.h
 Author      : Taygun Kekec
 Version     : 1.0
 Date		 : March 2013
 Copyright   : GNU
 Description :
			   This file covers common shared memory usage
			   definitions and functions for easy read/write operations
 ============================================================================
 */

#ifndef SUSHMLIB_H_
#define SUSHMLIB_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>		//Unix Interprocess Communication Library
#include <sys/shm.h>		//Unix Shared Memory Library

/* Shared Memory Settings */
#define SHARED_MEMORY_SIZE 4096
#define SHARED_MEMORY_KEY  6666
#define SHARED_MEMORY_INIT_VAL  0

/* Shared Memory Access Macros */

class SM{
public:
	static const int systemFailure;
	static const int platformType;
	static const int sysStatus;
	static const int adcTest;
	static const int serial1Test;
	static const int serial2Test;
	static const int rfControlToggle;
	static const int computerControlToggle;

	static const int RFDuty1, RFDuty2, RFDuty3, RFDuty4;
	static const int xPos, yPos, zPos;


	class QR{
	public:
		static const int roll, pitch, yaw;	// Attitude angles
		static const int rollRate, pitchRate, yawRate;
		static const int kpRoll, kdRoll, kiRoll;
		static const int kpPitch, kdPitch, kiPitch;
		static const int kpYaw, kdYaw, kiYaw;
		static const int motorDuty1, motorDuty2, motorDuty3, motorDuty4;
		static const int U1, U2, U3, U4;
		static const int ADC1, ADC2, ADC3, ADC4, ADC5, ADC6, ADC7, ADC8;
		static const int quadPidTest;
		static const int w1gain, w2gain, w3gain, w4gain;
		static const int desAltitude;
		// state variables
	};
};

class DEFS{
	public:
	class PLATFORM{
	public:
		enum { QUADROTOR, TRICOPTER, MOBILE_ROBOT, BLIMP };
	};

	class STATES{
	public:
		enum { STOPPED, PAUSED, RUNNING, MAINTENANCE };
	};
};

int bindSharedMem(char **shm);
int allocateSharedMem(char **shm);

void writeToSM(char *shm, int address, float value);
void writeToSM(char *shm, int address, int value);
void writeToSM(char *shm, int address, char value);

/*******************************************************************************
* Function Name  : getSM
* Input          : pointer to shared memory start
* 				   address to access ( definitions in suShmLib )
* Return         : corresponding pointer
* Description    : Reads desired variable from shared memory to the destination
*******************************************************************************/
inline void* getSM(char *shm, int address ) { return (void*)&shm[address];  };

#endif /* DEFINITIONS_H_ */
