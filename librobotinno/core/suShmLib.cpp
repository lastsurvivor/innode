/*
 ============================================================================
 Name        : suShmLib.c
 Author      : Taygun Kekec
 Version     : 1.0
 Date		 : March 2013
 Copyright   : GNU
 Description :
			   This file covers global definitions in the software such as:

			   -memory locations for shared memory access for different platforms( quadrotors, mobile robots)
 ============================================================================
 */

#include "suShmLib.h"


/* Definitions and Macros
 *
 */


/* Variable Locations in Shared Memory
 *
 * WARNING: THESE REPRESENT THE LOCATION OFFSET IN SHARED MEMORY, NOT THE VALUES OF THE VARIABLES
 *
 * Increment +4 for float vals
 * Increment +4 for integers
 * Increment +1 for chars
 */


/* Platform Independent, System State */
const int SM::systemFailure = 256;			// 1 byte char
const int SM::platformType = 257;			// 1 byte char
const int SM::sysStatus = 258;				// 1 byte char
const int SM::adcTest = 259;				// 1 byte char
const int SM::serial1Test = 260;			// 1 byte char
const int SM::rfControlToggle = 261;		// 1 byte char
const int SM::computerControlToggle = 262;	// 1 byte char
const int SM::RFDuty1 = 263;				// 4 byte integer
const int SM::RFDuty2 = 267;				// 4 byte integer
const int SM::RFDuty3 = 271;				// 4 byte integer
const int SM::RFDuty4 = 275;				// 4 byte integer
const int SM::xPos = 279;						// 4 byte float
const int SM::yPos = 283;						// 4 byte float
const int SM::zPos = 287;						// 4 byte float
const int SM::serial2Test = 291;			    // 1 byte char

/* Quadrotor Platform */
const int SM::QR::roll = 512;
const int SM::QR::pitch = 516;
const int SM::QR::yaw = 520;
const int SM::QR::motorDuty1 = 524;
const int SM::QR::motorDuty2 = 528;
const int SM::QR::motorDuty3 = 532;
const int SM::QR::motorDuty4 = 536;
const int SM::QR::U1 = 540;
const int SM::QR::U2 = 544;
const int SM::QR::U3 = 548;
const int SM::QR::U4 = 552;
const int SM::QR::ADC1 = 556;
const int SM::QR::ADC2 = 560;
const int SM::QR::ADC3 = 564;
const int SM::QR::ADC4 = 568;
const int SM::QR::ADC5 = 572;
const int SM::QR::ADC6 = 576;
const int SM::QR::ADC7 = 580;
const int SM::QR::ADC8 = 584;
const int SM::QR::quadPidTest = 588;	// 1 Byte toggle
const int SM::QR::w1gain = 589;			// 4 Byte float
const int SM::QR::w2gain = 593;			// 4 Byte float
const int SM::QR::w3gain = 597;			// 4 Byte float
const int SM::QR::w4gain = 601;			// 4 Byte float
const int SM::QR::rollRate = 605;		// 4 Byte float
const int SM::QR::pitchRate = 609;		// 4 Byte float
const int SM::QR::yawRate = 613;		// 4 Byte float
const int SM::QR::kpRoll = 617;
const int SM::QR::kdRoll = 621;
const int SM::QR::kiRoll = 625;
const int SM::QR::kpPitch = 629;
const int SM::QR::kdPitch = 633;
const int SM::QR::kiPitch = 637;
const int SM::QR::kpYaw = 641;
const int SM::QR::kdYaw= 645;			// 4 BYTE FLOAT
const int SM::QR::kiYaw = 649;			// 4 BYTE FLOAT
const int SM::QR::desAltitude = 653;     // 4 BYTE FLOAT


/*******************************************************************************
* Function Name  : writeToSM
* Input          : address ( Defined in suShmLib ) ,  value ( int  )
* Output         : Updated Shared Memory
* Return         : None
* Description    : Writes given value to the shared memory
*******************************************************************************/
 void writeToSM(char *shm, int address, int value)
{
	//memset(&shm[address], value,  sizeof(int) );

	shm[address] = ((char*)(&value))[0];
	shm[address+1] = ((char*)(&value))[1];
	shm[address+2] = ((char*)(&value))[2];
	shm[address+3] = ((char*)(&value))[3];
}

 /*******************************************************************************
 * Function Name  : writeToSM
 * Input          : address ( Defined in suShmLib ) ,  value (  float )
 * Output         : Updated Shared Memory
 * Return         : None
 * Description    : Writes given value to the shared memory
 *******************************************************************************/
 void writeToSM(char *shm, int address, float value)
{
		shm[address] = ((char*)(&value))[0];
		shm[address+1] = ((char*)(&value))[1];
		shm[address+2] = ((char*)(&value))[2];
		shm[address+3] = ((char*)(&value))[3];
}

 /*******************************************************************************
 * Function Name  : writeToSM
 * Input          : address ( Defined in suShmLib ) ,  value (  char )
 * Output         : Updated Shared Memory
 * Return         : None
 * Description    : Writes given value to the shared memory
 *******************************************************************************/
 void writeToSM(char *shm, int address, char value)
{
		shm[address] = value;
}

 int bindSharedMem(char **shm)
 {
	void *securityPtr;
	key_t key;
	int shmid;

	/* Get the segment named "5678", created by the server. */
	key = SHARED_MEMORY_KEY;
	/* Locate the segment */
	if ( ( shmid = shmget(key, SHARED_MEMORY_SIZE, 0666)) < 0) {
		perror("shmget::bindSharedMem");
		return -1;
	}
	/* Attach the segment to our data space. */
	if ((securityPtr = shmat(shmid, 0, 0)) == (char *) -1) {
		perror("shmat::bindSharedMem");
		return -1;
	}
	*shm = static_cast<char *>(securityPtr);
 }

 int allocateSharedMem(char **shm)
 {
	void *securityPtr;
	key_t key;
	int shmid;

	/* Get the segment named "5678", created by the server. */
	key = SHARED_MEMORY_KEY;
	/* Locate the segment */
	if ( ( shmid = shmget(key, SHARED_MEMORY_SIZE, IPC_CREAT | 0666)) < 0) {
		perror("shmget::allocateSharedMem");
		return -1;
	}
	/* Attach the segment to our data space. */
	if ((securityPtr = shmat(shmid, 0, 0)) == (char *) -1) {
		perror("shmat::allocateSharedMem");
		return -1;
	}

	*shm = static_cast<char *>(securityPtr);
 }
