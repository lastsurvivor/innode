#ifndef PACKAGE_H
#define PACKAGE_H

#include "PackageConstants.h"

class Package
{
public:
     char tag;
     unsigned short length;
     char value[PCK_LEN];

     Package();
     void printPackage();
     int serialize(char *dst)const;
     int deserialize(char *dst);
     int initialize(int modifier);
     virtual ~Package();
};

#endif // PACKAGE_H
