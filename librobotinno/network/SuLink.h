#include "../tiC2000.h"						// Microcontroller Communication
#include "../core/Utility.h"				// Microcontroller Communication
#include <iostream>
#include <stdio.h>
#include <string.h>				/* String function definitions */
#include <unistd.h>				/* UNIX standard function definitions */
#include <fcntl.h>				/* File control definitions */
#include <errno.h>				/* Error number definitions */
#include <termios.h>			/* POSIX terminal control definitions */

using namespace std;

void open_serial(int *fd, const char *serialDevice);
void writeRetry(int *fd, char *buffer, int len);
void sendCmdPacket(int *fd, int packageType);
void sendDataPacket(int *fd, int packageType, int len, char *data);
void setPIDGains(int *fd, int type, float Kp, float Kd, float Ki);
void setSonarGains(int *fd, int type, float Kp, float Kd, float Ki);
void setMotorGains(int *fd, int type, float gain1, float gain2);
void setCompFilterValue(int *fd, int type, float value);
int mainSuLinkTest();
